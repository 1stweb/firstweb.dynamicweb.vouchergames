# README #

This is a Custom module for Dynamicweb supposed to be the backend for HTML5 Voucher driven games such as Scratchcards and Slotmachines.

### What is this repository for? ###

* Managing Vouchergames and voucherlists for html5 games.

### How do I get set up? ###

* Clone this repo into /CustomModules in your Dynamicweb installation.
* Move playgame.asmx to /admin/public/webservices -- Apparantly webservices are only meant to be run from here??
* Games put in the front end consuming the service ex. http://HOST.DK/admin/webservices/playgame.asmx/play
* this will return a string array with a bool winorlose, string message
* If true it will pluck a voucher from the list, and send it through the webservice.
* Profit!


### Who do I talk to? ###

* jakob Kristensen
* jakob@1stweb.dk
﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dynamicweb.eCommerce.Orders.SalesDiscounts;
using Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.install;
using Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.lib;

namespace Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames
{
    public partial class Slotmachine : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dwInfoBar.Visible = false;
            if (!IsPostBack)
            {
                foreach (var voucherListItem in VoucherList.GetAllVoucherLists())
                {
                    var voucherItem = new ListItem();
                    voucherItem.Text = voucherListItem.ListName;
                    voucherItem.Value = voucherListItem.ID.ToString();
                    dwUIlitVoucherList.Items.Add(voucherItem);
                }

                var settings = SlotmachineGame.SlotmachineSettings();
                var count = Voucher.GetAllVouchersForList(settings.VoucherListId).Count(v => v.Status != "sent");
                dwUIlitVouchersLeft.Text = count.ToString();
                dwUIlitVoucherList.SelectedValue = settings.VoucherListId.ToString();
                dwUIgameActive.Checked = settings.Active;
                dwUIlitPlayerTries.Text = settings.NumberOfTries.ToString();
                dwUItxtwinPct.Text = settings.Winpct.ToString();   
            }
            
        }

        protected void OnClick(object sender, EventArgs e)
        {
            if (dwUIlitVoucherList.SelectedValue != "")
            {
                var selectedvalue = 0;
                int.TryParse(dwUIlitVoucherList.SelectedItem.Value, out selectedvalue);
                if (selectedvalue != 0)
                {
                    if (dwUItxtwinPct.Text != "")
                    {
                        if (dwUIlitPlayerTries.Text != "")
                        {
                            var winpct = -1;

                            int.TryParse(dwUItxtwinPct.Text, out winpct);
                            if (winpct == -1)
                            {
                                dwInfoBar.Message = @"Indsæt et tal i vinder procent..";
                                dwInfoBar.Visible = true;
                                return;
                            }
                            var playerTries = -1;
                            int.TryParse(dwUIlitPlayerTries.Text, out playerTries);

                            if (playerTries == -1)
                            {
                                dwInfoBar.Message = @"Indsæt et tal i spiller forsøg..";
                                dwInfoBar.Visible = true;
                                return;
                            }


                            var selectedVoucherList = VoucherList.GetListByID(selectedvalue);

                            int gameActive;
                            gameActive = dwUIgameActive.Checked ? 1 : 0;

                            SlotmachineGame.UpdateSlotmachineSettings(winpct, playerTries, selectedVoucherList.ID,
                                gameActive);
                            Response.Redirect("Slotmachine.aspx");
                            //dwUIlitVouchersLeft.Text = count.ToString();
                            //Voucher voucher = Voucher.GetAllVouchersForList(selectedVoucherList.ID).OrderBy(x => x.ID).Last(x => x.Status != "sent");
                            //dwUIlitVouchersLeft.Text += voucher.Code;
                            //voucher.Status = "sent";
                            //voucher.Save();
                        }
                        else
                        {
                            dwInfoBar.Message = @"Indsæt antal forsøg pr dag.";
                            dwInfoBar.Visible = true;
                        }
                    }
                    else
                    {
                        dwInfoBar.Message = @"Indsæt vinder procent pr. spil";
                        dwInfoBar.Visible = true;
                    }
                }
            }
            else
            {
                dwInfoBar.Message = @"Vælg en voucherliste..";
                dwInfoBar.Visible = true;
            }
        }
    }
}
﻿using System;
using System.Data;
using System.Data.SqlClient;
using Dynamicweb;
using Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.lib;

namespace Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.install
{
    [Dynamicweb.Extensibility.Subscribe(Dynamicweb.Notifications.Standard.Application.Start)]
    public class Vouchergames : Dynamicweb.Extensibility.NotificationSubscriber
    {
        public override void OnNotify(string notification, Dynamicweb.Extensibility.NotificationArgs args)
        {
            if (args == null)
                return;

            if (!(args is Dynamicweb.Notifications.Standard.Application.StartArgs))
                return;

            Dynamicweb.Notifications.Standard.Application.StartArgs item =
                (Dynamicweb.Notifications.Standard.Application.StartArgs) args;

            if (!ModuleExists("firstweb.dynamicweb.vouchergames"))
            {
                //then register that monkey!
                RegisterModule("firstweb.dynamicweb.vouchergames", "Voucher Games", true, false,
                    "/CustomModules/firstweb.dynamicweb.vouchergames/Overview.aspx", false);
            }

            if (!Database.TableExists("FirstwebVoucherSlotMachine", "Dynamicweb.mdb"))
            {
                SlotmachineGame.CreateSlotmachineTable();
               SlotmachineGame.RegisterSlotmachineSettings(30, 3, 9, 0);
            }

            if (!Database.TableExists("FirstwebSlotUser", "Dynamicweb.mdb"))
            {
                SlotmachineGame.CreateSlotUserTable();
            }

        }

        public void RegisterModule(string systemName, string name, bool access, bool standard, string script,
            bool paragraph)
        {
            using (IDbConnection connection = Dynamicweb.Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            @"INSERT INTO Module (ModuleSystemName, ModuleName, ModuleAccess, ModuleStandard, ModuleScript, ModuleParagraph)" +
                            " VALUES " +
                            "(@SystemName, @Name, @Access, @Standard, @script, @paragraph)";
                        command.Parameters.Add(new SqlParameter("@SystemName", systemName));
                        command.Parameters.Add(new SqlParameter("@Name", name));
                        command.Parameters.Add(new SqlParameter("@Access", access));
                        command.Parameters.Add(new SqlParameter("@Standard", standard));
                        command.Parameters.Add(new SqlParameter("@script", script));
                        command.Parameters.Add(new SqlParameter("@paragraph", paragraph));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {

                        throw;
                    }

                }
            }
        }

        public bool ModuleExists(string systemname)
        {


            using (IDbConnection connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            "SELECT COUNT(DISTINCT ModuleSystemName) FROM Module WHERE ModuleSystemName = @systemname";
                        command.Parameters.Add(new SqlParameter("@systemname", systemname));
                        command.ExecuteScalar();
                        int parseout = -1;

                        if (int.TryParse(command.ExecuteScalar().ToString(), out parseout))
                        {
                            if (parseout > 0)
                                return true;
                            else return false;
                        }


                    }
                    catch (SqlException sqlException)
                    {

                    }

                }
            }

            return false;
        }

    }


}


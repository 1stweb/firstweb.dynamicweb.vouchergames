﻿using System;
using System.Web.Services;
using Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.lib;

namespace Firstweb.Custom
{
    /// <summary>
    /// Summary description for PlayGame
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PlayGame : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            var slotSettings = SlotmachineGame.SlotmachineSettings();
            Random randomGenerator = new Random();

            //is this 100%% need mathmatician ^^
            var randomNr = randomGenerator.Next(0, 100);
            if (randomNr < slotSettings.Winpct)
            {
                return "You win!";
            }
            else
            {
                return "You lost";
            }
            
            
        }
    }
}

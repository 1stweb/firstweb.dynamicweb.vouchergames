﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomModules/firstweb.dynamicweb.vouchergames/Template.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.Overview" %>

<%@ Register TagPrefix="dw" Namespace="Dynamicweb.Controls" Assembly="Dynamicweb.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <dw:ControlResources ID="cr1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <dw:RibbonBar runat="server" ID="dwRibbonbar">
        <dw:RibbonBarTab runat="server" ID="dwUIGeneralTab" Name="Options">
            <dw:RibbonBarGroup runat="server" ID="dwUINewCampaignGroup" Name="Games">
                <dw:RibbonBarButton runat="server" Size="Large" Image="EditGear" ID="AdformUINewButton" Title="Games" Text="Slotmachine" OnClientClick="location.href='Slotmachine.aspx';"/>
                
            </dw:RibbonBarGroup>
        </dw:RibbonBarTab>
    </dw:RibbonBar>
  
</asp:Content>

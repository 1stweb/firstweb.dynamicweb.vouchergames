﻿using System;
using System.Data.SqlClient;
using System.Web.Services;
using Dynamicweb;

namespace Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.lib
{
    public class SlotmachineGame
    {
        public static void RegisterSlotmachineSettings(int winpct, int numberOfTries, int voucherListId, int active)
        {
            using (var connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            @"INSERT INTO FirstwebVoucherSlotMachine (winpct, numberoftries, voucherlistid, active)" +
                            " VALUES " +
                            "(@winpct, @numberoftries, @voucherlistid, @active)";
                        command.Parameters.Add(new SqlParameter("@winpct", winpct));
                        command.Parameters.Add(new SqlParameter("@numberoftries", numberOfTries));
                        command.Parameters.Add(new SqlParameter("@voucherlistid", voucherListId));
                        command.Parameters.Add(new SqlParameter("@active", active));

                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public static void UpdateSlotmachineSettings(int winpct, int numberOfTries, int voucherListId, int active)
        {
            using (var connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText =
                            @"UPDATE FirstwebVoucherSlotMachine
                                                SET winpct = @winpct, numberoftries = @numberoftries, voucherlistid = @voucherlistid, active = @active " +
                            " WHERE Id = 1";
                        command.Parameters.Add(new SqlParameter("@winpct", winpct));
                        command.Parameters.Add(new SqlParameter("@numberoftries", numberOfTries));
                        command.Parameters.Add(new SqlParameter("@voucherlistid", voucherListId));
                        command.Parameters.Add(new SqlParameter("@active", active));

                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        //table for each user connecting and checking if ip already is used.
        public static void CreateSlotUserTable()
        {
            using (var connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = @"CREATE TABLE FirstwebSlotUser(
                                                    Id INT IDENTITY PRIMARY KEY NOT NULL,
                                                    userip varchar(50) NOT NULL,
                                                    vouchercode varchar(50),
                                                    usertries INT NOT NULL,
                                                    lasttry DATETIME NOT NULL)";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public static void CreateSlotmachineTable()
        {
            using (var connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = @"CREATE TABLE FirstwebVoucherSlotMachine(
                                                    Id INT IDENTITY PRIMARY KEY NOT NULL,
                                                    winpct INT NOT NULL,
                                                    numberoftries INT NOT NULL,
                                                    voucherlistid INT NOT NULL,
                                                    active BIT NOT NULL)";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }

        public static SlotmachineSetting SlotmachineSettings()
        {
            var settings = new SlotmachineSetting();

            using (var connection = Database.CreateConnection("Dynamicweb.mdb"))
            {
                using (var command = connection.CreateCommand())
                {
                    try
                    {
                        command.CommandText = @"SELECT * 
                                                FROM FirstwebVoucherSlotMachine
                                                WHERE Id = 1";
                        command.ExecuteNonQuery();
                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                settings.Winpct = Convert.ToInt32(reader["winpct"]);
                                settings.NumberOfTries = Convert.ToInt32(reader["numberoftries"]);
                                settings.VoucherListId = Convert.ToInt32(reader["voucherlistid"]);
                                settings.Active = Convert.ToBoolean(reader["active"]);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return settings;
        }

        public class SlotmachineSetting
        {
            public int Id { get; set; }
            public int Winpct { get; set; }
            public int NumberOfTries { get; set; }
            public int VoucherListId { get; set; }
            public bool Active { get; set; }
        }

    }
}
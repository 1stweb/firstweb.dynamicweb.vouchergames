﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames {
    
    
    public partial class Overview {
        
        /// <summary>
        /// cr1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dynamicweb.Controls.ControlResources cr1;
        
        /// <summary>
        /// dwRibbonbar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dynamicweb.Controls.RibbonBar dwRibbonbar;
        
        /// <summary>
        /// dwUIGeneralTab control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dynamicweb.Controls.RibbonBarTab dwUIGeneralTab;
        
        /// <summary>
        /// dwUINewCampaignGroup control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dynamicweb.Controls.RibbonBarGroup dwUINewCampaignGroup;
        
        /// <summary>
        /// AdformUINewButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Dynamicweb.Controls.RibbonBarButton AdformUINewButton;
    }
}

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/CustomModules/firstweb.dynamicweb.vouchergames/Template.Master" AutoEventWireup="true" CodeBehind="Slotmachine.aspx.cs" Inherits="Firstweb.Custom.CustomModules.firstweb.dynamicweb.vouchergames.Slotmachine" %>
<%@ Register TagPrefix="dw" Namespace="Dynamicweb.Controls" Assembly="Dynamicweb.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <dw:ControlResources ID="cr1" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <dw:RibbonBar runat="server" ID="dwRibbonbar">
        <dw:RibbonBarTab runat="server" ID="dwUIGeneralTab" Name="Options">
            <dw:RibbonBarGroup runat="server" ID="dwUINewCampaignGroup" Name="Actions">
                <dw:RibbonBarButton runat="server" Size="Large" Image="LineChart" ID="dwUIupdateButton" Title="Tilføj liste" Text="Update" EnableServerClick="True" OnClick="OnClick" />
             
            </dw:RibbonBarGroup>
        </dw:RibbonBarTab>
    </dw:RibbonBar>
  
   	<dw:InfoBar ID="dwInfoBar" runat="server" Visible="False" />

      <dw:GroupBox Title="Slotmachine" runat="server">
        <table>
           <tr>
                <td class="Label">Spil Aktiveret</td>
                <td>
                    <asp:CheckBox ID="dwUIgameActive" runat="server"  />
                </td>
            </tr>
              <tr>
                <td class="Label">Win % pr spin</td>
                <td>
                    <asp:TextBox ID="dwUItxtwinPct" runat="server" CssClass="std" Width="100" />
                </td>
            </tr>

            <tr>
                <td class="Label">Antal forsøg
                </td>
                <td>
                    <asp:TextBox CssClass="std" Width="100" runat="server" ID="dwUIlitPlayerTries" />
                </td>
            </tr>
            <tr>
                <td class="Label">Antal vouchers tilbage:
                </td>
                <td>
                    <asp:Literal runat="server" ID="dwUIlitVouchersLeft" />
                </td>
            </tr>
            <tr>
                <td class="Label">Tilføj vouchers til spil:
                </td>
                <td>
                    <asp:ListBox runat="server" id="dwUIlitVoucherList" Height="100"/>                    
                </td>
            </tr>
           
        </table>
    </dw:GroupBox>
</asp:Content>
